import threading


class Singleton(type):
    """
    A base class for Singleton object.

    This class provide basic of singleton.
    Just set this class as metaclass to make it Singleton.

    It hopefully thread safe.
    """
    _instances: dict = {}
    _lock = threading.Lock()

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            with cls._lock:
                if cls not in cls._instances:
                    cls._instances[cls] = \
                        super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class ShinraUtilError(Exception):
    """
    Base class of error raised in this library.

    If you want to catch all errors caused by this library,
    write ``except ShinraUtilError:``.
    """
    pass
