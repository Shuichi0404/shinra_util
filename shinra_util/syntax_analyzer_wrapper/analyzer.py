__doc__ = """
Analyzer module.

This module provide some way to analyze string using KNP.
You should make KNPWrapper's instance first to use your own setting.

Currently, the offset this module returns is normal (simple) one.
That means it includes html tag strings length.
If you want to change this behavior, please check following code
and add some line to it.
"""


import logging

from pyknp import KNP
from pyknp.knp.blist import BList
from typing import List
import mojimoji
from shinra_util import virtuals
from shinra_util.syntax_analyzer_wrapper import parser

logger = logging.getLogger(__name__)
logger.setLevel(logging.WARNING)
logger.addHandler(logging.NullHandler())


def analyze_document(document: str) -> list:
    """
    Analyze whole document.

    Parameters
    ----------
    document: str
        html string or some other string include newline(s).

    Returns
    -------
    analyzed_data: list
        A list consists of each line's analyzed data.
        If an element is null list,
        it shows that that line has no target sentence.

    See Also
    --------
    analyze_line: Analyze each line.
    """
    logger.info("Analysis of document...")
    analyzed_data = [
        analyze_line(line) for line in document.split("\n")
    ]
    return analyzed_data


def analyze_line(line: str) -> list:
    """
    Analyze a line (means not include a newline charcter).

    Parameters
    ----------
    line: str
        String shows line content that has no newlines.

    Returns
    -------
    analyzed_line: list
        A list consists of analyzed object.
        It is an empty list if there is no target sentence in line.
    """
    logger.debug("Analysis of line...")
    partial_strings = parser.parse_line(line)
    logger.debug("parse line done")
    if len(partial_strings) == 0:
        return []
    analyzed_line = []
    for (_, sentence, (start, end)) in partial_strings:
        try:
            analyzed_content = analyze_sentence(sentence, start, end)
            analyzed_line.append(analyzed_content)
        except ValueError as e:
            logger.error(f"'{sentence}' in '{line}'")
            logger.error(f"position: {start, end}")
            raise e
    return analyzed_line


def analyze_sentence(sentence: str, start: int, end: int, *,
                     prefix: str = "") -> dict:
    """
    Analyze each ``sentence`` in a line.

    Parameters
    ----------
    sentence: str
        A target string to analyze.
        It can contain some parentheses,
        but it can not contain one or more separators.
        Separators is ["[ 　]+", "[1-9][0-9]+px"].
        In contrast of that,
        it can contain only one "。" as the last character.
    start: int
        The starting position of ``sentence`` (in the line).
    end: int
        The ending position of ``sentence`` (in the line).
    prefix: str
        Prefix string for name of sub sentences.
        For recursive use parameter.

    Returns
    -------
    analyzed_object: dict
        Analyzed object of ``sentence``.
        It consists of original string, replaced string,
        starting position, ending position, result of analysis,
        and result of sub sentence(s).
    """
    logger.debug(f"analyze sentence: '{sentence}'")
    (replaced, replaced_list) = han_to_zen(sentence)
    logger.debug(f"converted to '{replaced}'")
    rule = parser.ExcludeRules().rule
    (replaced, sub_sentences, exc_list, updates) = \
        parser.factor_out_and_exclude_by_rules(
            replaced, sentence, rule, prefix=prefix
    )
    logger.debug(f"factor out: {replaced}")
    logger.debug(f"separate sub sentences")
    sub_targets = \
        sum([parser.separate_string_by_rules(
            text, parser.SeparateRules().rules,
            offset=start, prefix=name)
            for (name, text, (start, end)) in sub_sentences], [])
    logger.debug("done")
    subtext_obj = {}
    for (name, subsentence, (start, end)) in sub_targets:
        subtext_obj[name] = \
            analyze_sentence(subsentence, start, end, prefix=name)
    replaced_list = _update_replst(replaced_list, updates)
    (analyzed_string, analysis_replace_list) = \
        KNPWrapper().parse(replaced, replaced_list)
    escape_obj = {name: {"string": string,
                         "start": start,
                         "end": end}
                  for (name, string, (start, end)) in exc_list}
    analyzed_object = {
        "original": sentence,
        "string": replaced if replaced != sentence else None,
        "start": start,
        "end": end,
        "result": (analyzed_string, analysis_replace_list),
        "subtext": subtext_obj,
        "escaped": escape_obj
    }
    return analyzed_object


def _update_replst(position_list: list, update_list: list) -> list:
    """
    Update the replaced position list to care for exclude strings.

    Parameters
    ----------
    position_list: List[int]
        The list of position in string.
    update_list: List[tuple]
        The list of tuple contains position and minus offset.

    Returns
    -------
    updated_lst: List
        Updated ``position_list``.
    """
    updated_lst = []
    for pos in position_list:
        for (idx, off) in update_list:
            if pos >= idx:
                updated_lst.append(pos - off)
                break
        else:
            updated_lst.append(pos)
    return updated_lst


def han_to_zen(string: str) -> tuple:
    """
    Replace specified halfwidth characters to fullwidth.
    The list of characters is symbolic characters in ASCII code.


    Parameters
    ----------
    string: str
        A target string to convert halfwidth charcter to fullwidth.

    Returns
    -------
    replaced_string: str
        A string converted from ``string``.
    replaced_positions: list
        A list of replaced position list.
    """
    def __conv_char(char):
        char2 = mojimoji.han_to_zen(char)
        ordchar = ord(char)
        if not (0x30 <= ordchar <= 0x39) and (char != char2):
            return (char2, True)
        else:
            return (char, False)

    parens = []
    char_list = []
    replaced_list = []
    rule = parser.ExcludeRules().rule
    skip = -1
    for (idx, char) in enumerate(string):
        if char in {"｢", "「", "『"}:
            parens.append((idx, char))
            logger.debug(f"find paren: {char}")
        elif len(parens) > 0 and char == chr(ord(parens[-1][1]) + 1):
            logger.debug(f"pop paren: {char}")
            parens.pop()
        if len(parens) == 0 and rule.match(string[idx:]):
            mt = rule.match(string[idx:])
            skip = idx + mt.end()
            logger.debug(f"Matchs exclude rule. {idx, skip, mt.group()}")
        elif (idx > skip) and (len(parens) == 0):
            (char, conv) = __conv_char(char)
            if conv:
                logger.debug(
                    f"replaced: '{char}' "
                    + f"idx: {idx} "
                    + f"around: '{string[idx-2:idx+2]}' "
                )
                replaced_list.append(idx)
            char_list.append(char)
        elif char == "｢":
            char_list.append("「")
        else:
            char_list.append(char)
    if len(parens) != 0:
        pos = parens[0][0]
        ss = string[pos:]
        st = mojimoji.zen_to_han(ss)
        rlist = [idx for (idx, (ch1, ch2)) in enumerate(zip(ss, st))
                 if ch1 != ch2]
        replaced_list.extend(rlist)
        replaced_list.sort()
        replaced_string = "".join(char_list[:pos]) + st
    else:
        replaced_string = "".join(char_list)
    return (replaced_string, replaced_list)


class KNPWrapper(metaclass=virtuals.Singleton):
    """
    Wrapper class of KNP that is Japanese syntax analyzer.
    This is Singleton class. So, configured once,
    returns the same instance whether jdir is given or not.
    """
    _knp: KNP = None
    _TIMEOUT = 180

    def __init__(self, *, jdir="", kserver="", jserver=""):
        """
        Initializer of KNP Wrapper.

        Parameters
        ----------
        jdir: str
            jumanpp directory path.
        """
        if KNPWrapper._knp is None:
            # if jdir == "":
            #     logger.info(
            #         f"KNPWrapper is created with options"
            #         + f" timeout:{KNPWrapper._TIMEOUT}"
            #     )
            #     KNPWrapper._knp = KNP(timeout=KNPWrapper._TIMEOUT)
            # else:
            #     logger.info(
            #         f"KNPWrapper is created with options"
            #         + f" timeout:{KNPWrapper._TIMEOUT}"
            #         + f" dir:{jdir}")
            #     KNPWrapper._knp = KNP(
            #         jumanoption=f"--dir={jdir}",
            #         timeout=KNPWrapper._TIMEOUT
            #     )
            if kserver != "":
                knp_op = f"-tab -C {kserver}"
            else:
                knp_op = "-tab"
            if jserver != "":
                j_op = f"-C {jserver}"
            else:
                j_op = ""
            KNPWrapper._knp = KNP(timeout=KNPWrapper._TIMEOUT,
                                  jumanoption=j_op,
                                  option=knp_op,
                                  jumanpp=False)

    @staticmethod
    def parse(sentence: str, position_list: list) -> tuple:
        """
        Wrapper of knp.parse()

        Parameters
        ----------
        sentence: str
            A target string that knp will analyze.
            Must not contain specific characters listed in below.
            ([" ", "+", "*"])
        position_list: list
            A list consists of positions to replace.
            It requires a normalized version of ``sentence``.
            And returns reconverted version of KNP's result.

        Returns
        -------
        result_string: str
            A result that of ``sentence`` analyzed by KNP.
            This string's some characters may replaced before return.
        replaced_list: list
            A list consists of some positions
            that shows replaced positions in ``resut_string``.
        """
        logger.debug(f"KNP: '{sentence}'")
        parsed = KNPWrapper._knp.parse(sentence)
        logger.debug("KNP: done")
        result_string = parsed.all()
        (result_string, replaced_list) = KNPWrapper._replace_result(
            result_string, position_list, mojimoji.zen_to_han
        )
        logger.debug("Convert some han chars to zen.")
        return (result_string, replaced_list)

    @staticmethod
    def _replace_result(string: str, pos_lst: list, fn) -> tuple:
        """
        Replace specified halfwidth characters in KNP's result string
        to fullwidth.

        Parameters
        ----------
        string: str
            A string of KNP's result.
        pos_lst: list
            A lsit of position in ``sentence`` that is base of ``string``.
        fn: function
            A function requires only one character map to character.

        Returns
        -------
        replaced_string: str
            A string basis on ``string``.
        lst_replace: list
            A list of position in ``replaced_string``.
        """
        if len(pos_lst) == 0:
            return (string, [])
        target_idx = -1
        lst_string = string.split("\n")
        lst_idx = 0
        lst_replace = []
        logger.debug(f"pos_lst: {pos_lst}")
        for (idx, text) in enumerate(lst_string):
            logger.debug(f"{idx}: '{text}'")
            if len(pos_lst) == 0 or len(text) == 0:
                break
            if text[0] in {"+", "*", "#"}:
                # + 1 :: \n
                lst_idx += len(text) + 1
                continue
            morpheme = text[:text.find(" ")]
            logger.debug(f"morpheme: {morpheme}")
            while len(pos_lst) > 0 \
                    and pos_lst[0] <= target_idx + len(morpheme):
                logger.debug(f"(head, target_idx, len(morpheme)): "
                             + f"{pos_lst[0], target_idx, len(morpheme)}")
                pos = pos_lst[0] - (target_idx + 1)
                lmr = len(morpheme)
                char_pair = (morpheme[pos], fn(morpheme[pos]))
                try:
                    morpheme = \
                        morpheme[:pos] + fn(morpheme[pos]) \
                        + morpheme[pos+1:]
                except IndexError as e:
                    logger.error(f"Error in line {idx}, '{text}'")
                    logger.error(f"morpheme: '{morpheme}'")
                    logger.error(f"pos_lst: {pos_lst}, "
                                 + f"target_idx: {target_idx},"
                                 + f" pos: {pos}")
                    raise e
                reptext = (morpheme + " ") * 3 + text[(lmr+1) * 3:]
                lst_string[idx] = reptext
                lst_replace.append((lst_idx + pos, char_pair))
                lst_replace.append((lst_idx + pos + lmr + 1, char_pair))
                lst_replace.append((lst_idx + pos + (lmr + 1) * 2, char_pair))
                logger.debug(
                    f"add: {lst_idx+pos, lst_idx+pos+lmr+1}"
                    + f"{lst_idx + pos + (lmr+1) * 2}")
                pos_lst = pos_lst[1:]
            target_idx += len(morpheme)
            # + 1 :: \n
            lst_idx += len(text) + 1
            logger.debug(f"tgtidx: {target_idx}, lst_idx: {lst_idx}")
        replaced_string = "\n".join(lst_string)
        return (replaced_string, lst_replace)

    @staticmethod
    def load_result(result_string: str,
                    replist: List[int]) -> BList:
        restored = \
            KNPWrapper._restore_result(result_string, replist)
        result = KNPWrapper._knp.result(restored)
        return result

    @staticmethod
    def _restore_result(text: str, replist: List[tuple]) -> str:
        """
        Restore the KNP's result from replaced string.

        Parameters
        ----------
        text: str
            A result of knp but it is replaced some characters.
        replist: List[tuple]
            A list of position in ``text`` and pair of characters
            that shows (from, to)

        Returns
        -------
        text: str
            Restored result of analysis.
        """
        for (idx, _) in replist:
            text = text[:idx] \
                + mojimoji.han_to_zen(text[idx]) \
                + text[idx+1:]
        return text
