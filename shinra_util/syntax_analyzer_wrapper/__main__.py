import json
import logging
import sys
import traceback
from logging.handlers import TimedRotatingFileHandler
from typing import List

from docopt import docopt

from shinra_util.syntax_analyzer_wrapper import analyzer, parser

__doc__ = """{f} Wrapper of syntax analyzer program (KNP)
Usage:
  {f} (-h|--help)
  {f} list [-n num|--num-of-split num] [--prefix pref] [--suffix suff] <DIR>
  {f} (-f FILE|--file FILE) [-o OUTPUT|--output OUTPUT] [--jumanpp-dir JDIR] [--log-file LOG] [--knp-server ks] [--juman-server js]
  {f} (-s SRCDIR|--source-dir SRCDIR) (-d DIR|--destination DIR) [--jumanpp-dir JDIR] [--log-file LOG] [--knp-server ks] [--juman-server js]
  {f} (-l LIST|--list LIST) (-d DIR|--destination DIR) [--jumanpp-dir JDIR] [--log-file LOG] [--knp-server ks] [--juman-server js]

Options:
  -h --help  			  Show this help.
  list  			  Make file list helper (for --list option)
  -n num --num-of-split num  	  Number of split files. [default: 3]
  --prefix pref  	  Prefix of output files. Will be concatinated with file number. [default: list]
  --suffix suff  	  Suffix of output files. [default: .dat]
  <DIR>  			  Directory which have many html files.
  -f FILE --file FILE  		  Input file path
  -o OUTPUT --output OUTPUT  	  Output file path [default: stdout]
  -s SRCDIR --source-dir SRCDIR   The directory name which html files are in.
  -d DIR --destination DIR  	  The directory which you want to store results.
  -l LIST --list LIST  		  A list of files to analyze.
  --jumanpp-dir jdir  		  juman specific file directory. [default: /usr/local/share/jumanpp]
  --log-file LOG  		  File to save log. [default: syntax_analyzer.log]
  --knp-server kn  		  KNP Server option [default: ""]
  --juman-server js  		  Juman Server option [default: ""]
""".format(f=__file__)


logger = logging.getLogger(__name__)


def main(options):
    root_logger = logging.getLogger()
    handler = TimedRotatingFileHandler(
        filename=options["--log-file"],
        when="D",
        interval=1,
        backupCount=31,
    )
    formatter = logging.Formatter(
        '%(asctime)s %(name)s %(funcName)s [%(levelname)s]: %(message)s'
    )
    handler.setFormatter(formatter)
    root_logger.addHandler(handler)
    analyzer.KNPWrapper(jdir=options["--jumanpp-dir"],
                        kserver=options["--knp-server"],
                        jserver=options["--juman-server"])
    if options["list"]:
        try:
            _make_file_list(
                options["<DIR>"],
                int(options["--num-of-split"]),
                options["--prefix"],
                options["--suffix"]
            )
        except ValueError:
            logger.critical(
                f"--num-of-split is invalid. "
                + "Expect: int, Given: {options['--num-of-split']}")
            exit(1)
    try:
        if options["--file"]:
            _set_loglevel(logging.DEBUG)
            logger.info("single file mode")
            _for_single_file(
                options["--file"],
                options["--output"],
            )
        elif options["--list"]:
            _set_loglevel(logging.INFO)
            logger.info("list file mode")
            _for_list(
                options["--list"],
                options["--destination"]
            )
        else:
            _set_loglevel(logging.INFO)
            logger.info("directory mode")
            _for_directory(
                options["--source-dir"],
                options["--destination"],
            )
    except Exception as e:
        logger.error(f"Some error occured. {e.args}")
        logger.critical(traceback.format_exc())
        print(f"Some error occured. {e.args}", file=sys.stderr)
        exit(1)


def _make_file_list(tgt_dir: str, splits: int, prefix: str, suffix: str):
    import glob
    files = glob.glob(f"{tgt_dir}/*")
    ls_files: List[List[str]] = [[] for i in range(splits)]
    for (idx, fn) in enumerate(files):
        reminder = idx % splits
        ls_files[reminder].append(fn)
    for (idx, lst) in enumerate(ls_files):
        ofn = f"{prefix}{idx}{suffix}"
        logger.info(f"write file {ofn}")
        with open(ofn, "w") as wf:
            for fn in lst:
                print(fn, file=wf)


def _for_single_file(infn, outfn):
    try:
        with open(infn, "r") as in_file:
            html = in_file.read()
    except FileNotFoundError as e:
        logger.error("Input file can't find in specified path. "
                     + f"({infn})")
        print("Input file can't find in specified path. "
              + f"({infn})",
              file=sys.stderr)
        raise e
    logger.info(f"analyze file :{infn}")
    out_object = analyzer.analyze_document(html)
    logger.info("analyze file done")
    if outfn != "stdout":
        try:
            with open(outfn, "w") as out_file:
                print(json.dumps(out_object), file=out_file)
        except PermissionError as e:
            print("Get a permission error while openning output file.",
                  file=sys.stderr)
            raise e
    else:
        print(json.dumps(out_object), file=sys.stdout)


def _for_list(list_file_name, outdir):
    with open(list_file_name, "r") as list_file:
        file_names = list_file.read().split("\n")
    for ifn in file_names:
        if len(ifn) == 0:
            continue
        fn = ifn.split("/")[-1].split(".")[0]
        ofn = f"{outdir}/{fn}.knp.json"
        try:
            _for_single_file(ifn, ofn)
        except Exception as e:
            logger.error(e.args)
            logger.error(f"ERROR WITH FILE: '{ifn}'")


def _for_directory(indir, outdir):
    import glob
    files = glob.glob(f"{indir}/*")
    logger.info(f"for all files in {indir}")
    for ifn in files:
        fn = ifn.split("/")[-1].split(".")[0]
        ofn = f"{outdir}/{fn}.knp.json"
        try:
            _for_single_file(ifn, ofn)
        except Exception as e:
            logger.error(e.args)
            logger.error(f"ERROR WITH FILE: '{ifn}'")


def _set_loglevel(level):
    root_logger = logging.getLogger()
    root_logger.setLevel(level)
    logger.setLevel(level)
    analyzer.logger.setLevel(level)
    parser.logger.setLevel(level)


if __name__ == "__main__":
    main(docopt(__doc__))
