__doc__ = """
Parser module.

This module provides some parse methods and utilities for these.
"""


import logging
import re
from typing import List

import mojimoji
from shinra_util import virtuals

logger = logging.getLogger(__name__)
logger.setLevel(logging.WARNING)
logger.addHandler(logging.NullHandler())


def parse_line(line: str) -> list:
    """
    Parse ``line`` to some string(s).

    Parameters
    ----------
    line: str
        A string has no newline charcters.

    Returns
    -------
    sentences: list of tuple
        A list consists of tuple that contains text,
        start position, end position in order.
    """
    logger.debug(f"parse '{line}'")
    sentences: List[tuple] = []
    rules = SeparateRules().rules
    sentence_wrappers = _get_nontag_texts(line)
    sentences = sum([
        separate_string_by_rules(string, rules, offset=start)
        for (string, (start, _)) in sentence_wrappers
    ], [])
    sentences = sum([
        _separate_by_string(string, " / ", start)
        for (_, string, (start, _)) in sentences
    ], [])
    sentences = sum([
        _separate_by_string(string, " - ", start)
        for (_, string, (start, _)) in sentences
    ], [])
    for sentence in sentences:
        logger.debug(f"{sentence}")
    logger.debug(f"parse done.")
    return sentences


def _separate_by_string(string: str, separator: str, start: int = 0) -> list:
    """
    Separate given ``string`` by slash if there is two or more slashes.

    Parameters
    ----------
    string: str
        A target string.
    separator: str
        A string.
    start: int
        The starting point of the ``string``.
    """
    logger.debug(f"separate by '{separator}'")
    fs1 = string.find(separator)
    fs2 = string[fs1+1:].find(separator)
    fs3 = string[fs1 + fs2 + 2:].find(separator)
    if (fs1 == -1) or (fs2 == -1):
        logger.debug("there is not enough separator.")
        return [(None, string, (start, start + len(string)))]
    logger.debug("there is enough separator")
    strings: List[tuple] = []
    last_part = string
    pos = last_part.find(separator)
    while pos > 0:
        cur = start if len(strings) == 0 else strings[-1][2][1]
        text = last_part[:pos]
        strings.append((None, text, (cur, cur + len(text))))
        last_part = last_part[pos:]
        pos = last_part[1:].find(separator) + 1
    if len(last_part) > 0:
        text = last_part
        cur = start if len(strings) == 0 else strings[-1][2][1]
        strings.append((None, text, (cur, cur + len(text))))
    return strings


def _get_nontag_texts(string: str) -> list:
    """
    Get texts not include html tag(s) from ``string``.

    Parameters
    ----------
    string: str
        A string that you want to get texts from.

    Returns
    -------
    texts: list of tuple
        A list consists of tuple that has text,
        start position, end position like
        ``(text, (start_position, end_position))``
    """
    texts = []
    tag_end_idx = 0
    strlen = len(string) - 1
    for (idx, char) in enumerate(string):
        if char == "<" and tag_end_idx != idx:
            text = string[tag_end_idx:idx]
            logger.debug(f"find text: '{text}'")
            texts.append((text, (tag_end_idx, idx)))
        elif char == ">":
            tag_end_idx = idx + 1
        elif idx == strlen and (tag_end_idx != idx or idx == 0):
            texts.append((string[tag_end_idx:idx+1],
                          (tag_end_idx, idx+1)))

    return texts


def separate_string_by_rules(string: str, rules: list, *,
                             offset: int = 0,
                             prefix: str = "") -> list:
    """
    Separate string by regex in ``rules`` if not in parentheses.
    It return offset of text (in ``string`` + ``offset``)
    not only text.

    Parameters
    ----------
    string: str
        A target string separate to.
    rules: list of regex
        Rules for separate ``string``.
        It allows a list of string.
        In that case, this function compiles strings to re object.
    offset: int, default 0
        An offset to add into the starting and ending position
        of text in ``string``.

    Returns
    -------
    sentences: list of tuple
         A list consists of separated text(s) and
         the starting and ending position of ``text`` in ``string``,
         ``name`` of ``text``.
         ``name`` form is '{prefix}@{count}'
    """
    def __match_helper(matches, idx, last):
        if len(matches) == 1:
            end = idx + len(matches[0].group())
            return ((last, idx), (idx, end))
        else:
            logger.error("Multiple patterns are matched.")
            logger.error(f"in: '{string[idx:]}'")
            for mt in matches:
                logger.error(f"of: '{mt.group()}'")
            raise MatchError("Multiple patterns are matched.")

    logger.debug(f"ssr: offset={offset} '{string}'")
    names_cands = list(reversed([f"{prefix}@{chr(char)}" for char in
                                 list(range(ord('A'), ord('Z'))) +
                                 list(range(ord('a'), ord('z')))]))
    name = NameStore(prefix, names_cands).name
    rules = [re.compile(element) if type(element) is str else element
             for element in rules]
    last = 0
    sentences: List[tuple] = []
    string_len = len(string)
    parens: List[str] = []
    append = sentences.append
    logger.debug(f"len(string):{string_len}")
    for (idx, char) in enumerate(string):
        logger.debug(f"(last, idx):{last, idx}")
        matches = [rule.match(string[idx:]) for rule in rules
                   if rule.match(string[idx:])]
        if len(parens) == 0 and char == "。" and idx > last:
            text = string[last:idx+1]
            logger.debug(f"candidate maru: {last, idx+1} '{text}'")
            append((name(), text,
                    (last + offset, idx + 1 + offset)))
            last = idx + 1
        elif len(parens) == 0 and len(matches) > 0 and idx >= last:
            logger.debug(f"Matches pattern(s)")
            ((st1, ed1), (st2, ed2)) = __match_helper(matches, idx, last)
            if st1 != ed1:
                text = string[st1:ed1]
                logger.debug(f"candidate rule: {st1, ed1} '{text}'")
                append((name(), text, (st1 + offset, ed1 + offset)))
            text = string[st2:ed2]
            logger.debug(f"candidate rule: {st2, ed2} '{text}'")
            append((name(), text,
                    (st2 + offset, ed2 + offset)))
            last = ed2
        elif char in {"(", "（", "「", "『"}:
            parens.append(char)
        elif len(parens) > 0 and parens[-1] == chr(ord(char) - 1):
            parens.pop()
        if idx == string_len - 1 and idx >= last:
            text = string[last:idx+1]
            if len(text) == 0:
                continue
            logger.debug(f"candidate last: {last, idx+1} '{text}'")
            if len(sentences) == 0:
                append((prefix if prefix != '' else None, text,
                        (last + offset, idx + 1 + offset)))
            else:
                append((name(), text,
                        (last + offset, idx + 1 + offset)))
            last = idx + 1
    return sentences


def factor_out_and_exclude_by_rules(sentence: str, orig: str, rule, *,
                                    offset: int = 0, prefix: str = "") -> tuple:
    """
    Factor out texts from parentheses in ``sentence``.
    In ``sentence``, there can parentheses.
    In the way to syntax analysis, it makes no sense.
    So, we factor out these and analyze each
    as same as other basic sentences.

    Parameters
    ----------
    sentence: str
        A target string to factor out some text(s).
        It can include some parentheses.
    orig: str
        A original string of ``sentence``.
    rule:
        A rule for exclude string.
    offset: int
        An offset position.
        It will be added to sub sentence's offset,
        and the update list offset.
    prefix: str
        Prefix string for sub sentence's name.

    Returns
    -------
    replaced_sentence: str
        Replaced ``sentence``.
        Inner of arentheses are listed in ``subsentences``.
    subsentences: List
        Factors in ``sentence``.
        They are an innner of parentheses.
        Of cource, it presnet offset.
    updates: List
        If ``sentence`` is replaced, there may changes in offset.
        It present change log of offset.
    """
    def __helper(parens, start, end, name_count):
        if len(parens) == 0:
            text = sentence[start:end]
            name = f"{prefix}@{name_count}"
            return (name, text, (start + offset, end + offset))
        else:
            return (None, None, (None, None))
    logger.debug(f"fip: (off, pref)={offset, prefix} '{sentence}'")
    parens: List[str] = []
    start = -1
    name_count = 0
    exc_count = 0
    subsentences: List[tuple] = []
    exc_list: List[tuple] = []
    updates: List[tuple] = []
    tmp_upd = []
    for (idx, char) in enumerate(sentence):
        if char in {"｢", "「", "『"}:
            if len(parens) == 0:
                start = idx + 1
            parens.append(char)
            logger.debug(f"paren: '{char}', start: {start}")
        elif len(parens) > 0 and char == chr(ord(parens[-1]) + 1):
            parens.pop()
            logger.debug(f"close paren: '{char}'")
            logger.debug(
                f"(start, idx, name_count)={start, idx, name_count}")
            (name, text, (_start, end)) = \
                __helper(parens, start, idx, name_count)
            if name is None:
                logger.debug(f"no candidate")
                continue
            start = _start
            logger.debug(f"candidate: (name, text):{name, text}")
            subsentences.append((name, text, (start, end)))
            tmp_upd.append((start, end, name))
            current = 0 if len(updates) == 0 else updates[-1][1]
            logger.debug(f"upd: (text, name):({len(text), len(name)})")
            updates.append((end, current + len(text) - len(name)))
            name_count += 1
        elif len(parens) == 0 and rule.match(orig[idx:]):
            mt = rule.match(orig[idx:])
            start = idx
            end = idx+mt.end()
            name = f"{prefix}&{exc_count}"
            exc_count += 1
            cand = sentence[start:end]
            logger.debug(f"candidate: (name, text):({name}, '{cand}')")
            exc_list.append((name, cand, (start + offset, end + offset)))
            tmp_upd.append((start + offset, end + offset, name))
            current = 0 if len(updates) == 0 else updates[-1][1]
            logger.debug(f"upd: (text, name):({len(cand), len(name)})")
            updates.append((end + 1, current + len(cand) - len(name)))
    for (start, end, name) in reversed(tmp_upd):
        sentence = \
            sentence[:start - offset] + name + sentence[end - offset:]
        logger.debug(f"sentence -> '{sentence}'")
    return (sentence, subsentences, exc_list, updates)


def _han_to_zen_in_error(string, *, offset=0):
    """
    Replace all ``string`` if it is half width character.

    Parameters
    ----------
    string: str
        A target string.
    offset: int
        A offset of ``string``

    Returns
    -------
    replaced_string: str
        String based of ``string``
    replaced_list: List[int]
        A list of string positions that is replaced.
    """
    def __conv_char(char):
        char2 = mojimoji.han_to_zen(char)
        if char != char2:
            return (char2, True)
        else:
            return (char, False)

    replaced_list = []
    char_list = []
    for (idx, char) in enumerate(string):
        (char, conv) = __conv_char(char)
        if conv:
            replaced_list.append(idx + offset)
        char_list.append(char)
    replaced_string = "".join(char_list)
    return (replaced_string, replaced_list)


class SeparateRules(metaclass=virtuals.Singleton):
    """
    Manager of separate rules.
    This is a Singleton class.
    If you call ``SeparateRules()`` twice or more,
    all trials returns the same object.

    This manages ``SeparateRules.rules``.
    """
    rules: List = []

    def __init__(self):
        if SeparateRules.rules == []:
            logger.info("Separate rule manager created.")
            SeparateRules.rules = [
                re.compile(r"[ 　][ 　]+"),
                re.compile(r"[1-9][0-9]*px"),
            ]
        self.rules = SeparateRules.rules


class ExcludeRules(metaclass=virtuals.Singleton):
    """
    Exclude rule maanger.

    Used for exclude some substring from analysis target.
    """
    rule = None
    _rule_str = \
        r"((http|ftp)s?://[^ ]+)|" \
        + r"(ファイル:[^ ]+(jpg|JPG|jpeg|JPEG|png|PNG|gif|GIF|svg|SVG|tiff|TIFF))"

    def __init__(self):
        if ExcludeRules.rule is None:
            logger.info("Exclude rule manager created.")
            ExcludeRules.rule = re.compile(
                ExcludeRules._rule_str
            )
        self.rules = ExcludeRules.rule


class NameStore():
    """
    Store of sentence name.
    """

    def __init__(self, prefix: str, cands: List[str]):
        """
        Initialize NameStore.

        Parameters
        ----------
        prefix: str
            Prefix of name. It is only used to decision making.
            If it is '', ``self.name()`` returns None.
            Otherwise, return name from the store.
        cands: List[str]
            Candidates of name. It is a list of string.
        """
        self._cands = cands
        self._prefix = prefix

    def name(self):
        """
        Returns stored name from ``self._cands``.


        Returns
        -------
            None if ``prefix`` is '' else name from ``self.cands``.
            When number of stored name is zero and ``prefix`` is not '',
            it returns no value.

        Raises
        ------
        NameError:
            If stored name is nothing and prefix is not ''.
        """
        if self._prefix == '':
            return None
        elif len(self._cands) > 0:
            return self._cands.pop()
        else:
            raise NameError("I have no more name.")


class MatchError(virtuals.ShinraUtilError):
    """
    Shows that there is something wrong with separate rules.

    It commonly raised when multiple rules are matched
    in the point on the sentence.
    """
    pass


class NameError(virtuals.ShinraUtilError):
    """
    Shows that there is something wrong with sentence name.

    It commonly raised when the ``NameStore`` has no more name.
    """
    pass
