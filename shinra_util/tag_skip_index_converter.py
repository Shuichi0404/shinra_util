class TagSkipIndexConverter:
    def __init__(self, string):
        self.string = string
        self.__length = len(string)

    def get_tag_skip_index(self, idx):
        if idx < 0 or idx > self.__length:
            raise\
                IndexError(f"Index out of range: {idx} of {self.__length}")
        counter = 0
        skip = 0
        tag_flg = False
        if idx == 0 and self.string[0] != "<":
            return 0
        elif idx == 0:
            raise IndexError(f"specified index is in tag: {idx}")
        while counter < self.__length and counter < idx:
            if self.string[counter] == "<":
                tag_flg = True
            elif self.string[counter] == ">":
                tag_flg = False
            elif not tag_flg:
                skip += 1
            counter += 1
        if tag_flg:
            raise IndexError(f"specified index is in tag: {idx}")
        return skip

    def get_simple_index(self, idx):
        if idx < 0:
            raise IndexError(f"Index out of range: {idx}")
        counter = 0
        skip = -1
        tag_flg = False
        if idx == 0:
            while counter < self.__length:
                if self.string[counter] == "<":
                    tag_flg = True
                elif self.string[counter] == ">":
                    tag_flg = False
                elif not tag_flg:
                    return counter
                counter += 1
        while counter < self.__length and skip < idx:
            if self.string[counter] == "<":
                tag_flg = True
            elif self.string[counter] == ">":
                tag_flg = False
            elif not tag_flg:
                skip += 1
            if skip == idx:
                break
            counter += 1

        if tag_flg:
            raise IndexError(f"specified index is in tag: {idx}")
        return counter


class LineTagSkipIndexConverter:
    def __init__(self, string):
        self.length = len(string)
        self.lines = [line + "\n" for line in string.split("\n")]
        self.lines_len = []
        last = 0
        for (i, length) in \
                enumerate([len(line) for line in self.lines]):
            if i == 0:
                # line start char point
                self.lines_len.append(0)
            else:
                # line start char point
                start = last
                self.lines_len.append(start)
            last += length

    def string(self):
        return "".join(self.lines())

    def __get_max_nonover(self, idx):
        for (linum, length) in enumerate(self.lines_len):
            if length > idx:
                break
        return (linum - 1, self.lines_len[linum - 1])

    def get_line_skip_tag_index(self, idx):
        if idx < 0:
            raise IndexError(f"Index out of range: {idx}")
        elif idx >= self.length:
            raise IndexError(f"Index over size: {idx}")
        (linum, start) = self.__get_max_nonover(idx)
        skip_counter = TagSkipIndexConverter(self.lines[linum])
        counter = skip_counter.get_tag_skip_index(idx - start)
        return (linum, counter)

    def get_simple_index(self, idx):
        (linum, pos) = idx
        skip_counter = TagSkipIndexConverter(self.lines[linum])
        return self.lines_len[linum] + skip_counter.get_simple_index(pos)
