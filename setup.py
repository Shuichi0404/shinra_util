import os
import subprocess

from setuptools import Command, setup

PACKAGE_NAME = "shinra_util"


class SimpleCommand(Command):
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass


class VetCommand(SimpleCommand):
    def run(self):
        subprocess.check_call(["mypy", PACKAGE_NAME])
        subprocess.check_call(["flake8"])


class FmtCommand(SimpleCommand):
    def run(self):
        subprocess.call(["isort", "-y"])
        subprocess.call(["autopep8", "-ri", PACKAGE_NAME, "tests", "setup.py"])


class DocCommand(SimpleCommand):
    def run(self):
        opt = "-f" if os.path.exists(os.path.join("docs", "conf.py")) else "-F"
        subprocess.call(["sphinx-apidoc", opt, "-o", "docs", PACKAGE_NAME])
        if os.name == 'nt':
            subprocess.call([os.path.join("docs", "make.bat"), "html"])
        else:
            subprocess.call(["make", "-C", "docs", "html"])


setup(
    name="shinra_util",
    version="0.0.1",
    install_requires=["docopt", "mojimoji", "pyknp"],
    extras_require={
        "develop": ["flake8", "autopep8", "bpython", "mypy", "isort"]
    },
    cmdclass={
        "vet": VetCommand,
        "fmt": FmtCommand,
        "doc": DocCommand,
    },
)
