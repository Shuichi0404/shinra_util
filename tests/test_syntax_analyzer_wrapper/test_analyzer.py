import logging
import pprint
from unittest import TestCase

import pkg_resources

import mojimoji
from shinra_util.syntax_analyzer_wrapper import analyzer

get_handler = logging.FileHandler("logfiles/test.log")
analyzer.logger.addHandler(get_handler)
analyzer.logger.setLevel(logging.DEBUG)

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logger.addHandler(get_handler)


class TestAnalyzer(TestCase):

    def test_han_to_zen(self):
        logger.debug("test_han_to_zen")
        test_strings = [
            "なにか 文字列@1",
            "abcd",
            "-=()~^",
        ]
        expected_strings = [
            ("なにか　文字列＠１", [3, 7, 8]),
            ("ａｂｃｄ", [0, 1, 2, 3]),
            ('－＝（）～＾', [0, 1, 2, 3, 4, 5, ]),
        ]
        for (test, expected) in zip(test_strings, expected_strings):
            self.assertEqual(analyzer._han_to_zen(test), expected)
        logger.debug("clear")

    def test_analyze_sentence(self):
        def remove_result(obj: dict):
            if obj == {}:
                return obj
            if obj.get("result"):
                obj.pop("result")
            if obj.get("subtext"):
                obj["subtext"] = {
                    name: remove_result(value)
                    for (name, value) in obj["subtext"].items()
                }
            return obj

        def print_result(obj: dict):
            obj = remove_result(obj)
            pp = pprint.PrettyPrinter(indent=2)
            pp.pprint(obj)

        logger.debug("test_analyze_sentence")
        test_string = "何か文字列「XXXを示す「YY」」がある"
        actual = analyzer.analyze_sentence(test_string, 0, 20)
        print_result(actual)
        test_string = \
            "あ「ねこ http://sample.com」 from https://sample.com なり"
        actual = analyzer.analyze_sentence(test_string, 0, 50)
        print_result(actual)
        logger.debug("clear")


class TestKNPWrapepr(TestCase):

    def setUp(self):
        analyzer.KNPWrapper()

    def test_replace_result(self):
        logger.debug("test_replace_result")
        target = "森羅プロジェクト（2018年 - ）は" \
            + "理化学研究所革新知能統合研究センター（AIP）で行われる" \
            + "Wikipediaを構造化するプロジェクトである。"
        (zen_target, replaced_list) = analyzer._han_to_zen(target)
        base = pkg_resources.resource_string(
            "tests", "data/analyzer/riken.base.txt"
        ).decode()
        expected_string = pkg_resources.resource_string(
            "tests", "data/analyzer/riken.replaced.txt"
        ).decode()
        actual_obj = analyzer.KNPWrapper()._replace_result(
            base, replaced_list, mojimoji.zen_to_han
        )
        for (actual, expected) in zip(
                actual_obj[0].split("\n"),
                str(expected_string).split("\n")
        ):
            if len(expected) != 0 and expected[0] in {"#"}:
                continue
            self.assertEqual(actual, expected)
        self.assertEqual(
            actual_obj[1],
            # 2018, ' ', -, ' ', AIP, Wikipedia
            [966, 971, 976, 967, 972, 977, 968, 973, 978, 969, 974, 979,
             1261, 1263, 1265,
             1442, 1444, 1446,
             1664, 1666, 1668,
             4554, 4558, 4562, 4555, 4559, 4563, 4556, 4560, 4564,
             5882, 5892, 5902, 5883, 5893, 5903, 5884, 5894, 5904,
             5885, 5895, 5905, 5886, 5896, 5906, 5887, 5897, 5907,
             5888, 5898, 5908, 5889, 5899, 5909, 5890, 5900, 5910])
        logger.debug("clear")

    def test_parse(self):
        logger.debug("test_parse")
        target = "森羅プロジェクト（2018年 - ）は" \
            + "理化学研究所革新知能統合研究センター（AIP）で行われる" \
            + "Wikipediaを構造化するプロジェクトである。"
        (zen_target, replaced_list) = analyzer._han_to_zen(target)
        expected_string = pkg_resources.resource_string(
            "tests", "data/analyzer/riken.replaced.txt"
        ).decode()
        actual_obj = analyzer.KNPWrapper().parse(zen_target, replaced_list)
        for (actual, expected) in zip(
                actual_obj[0].split("\n"),
                str(expected_string).split("\n")
        ):
            if len(expected) != 0 and expected[0] in {"#", "+", "*"}:
                continue
            self.assertEqual(actual, expected)
        self.assertEqual(
            actual_obj[1],
            [966, 971, 976, 967, 972, 977, 968, 973, 978, 969, 974, 979,
             1261, 1263, 1265, 1442, 1444, 1446, 1664, 1666, 1668,
             4554, 4558, 4562, 4555, 4559, 4563, 4556, 4560, 4564,
             5882, 5892, 5902, 5883, 5893, 5903, 5884, 5894, 5904,
             5885, 5895, 5905, 5886, 5896, 5906, 5887, 5897, 5907,
             5888, 5898, 5908, 5889, 5899, 5909, 5890, 5900, 5910])
        logger.debug("clear")
