import logging
from unittest import TestCase

from shinra_util.syntax_analyzer_wrapper import parser

get_handler = logging.FileHandler("logfiles/test.log")
parser.logger.addHandler(get_handler)
parser.logger.setLevel(logging.DEBUG)

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logger.addHandler(get_handler)


class TestParser(TestCase):
    def test_parse_line(self):
        logger.debug("test_parse_line")
        test_string = "<h1>上記に加えて</h1>その後も文字列が  あったりする"
        expected = [
            (None, "上記に加えて", (4, 10)),
            (None, "その後も文字列が", (15, 23)),
            (None, "  ", (23, 25)),
            (None, "あったりする", (25, 31))
        ]
        actual = parser.parse_line(test_string)
        self.assertEqual(actual, expected)
        logger.debug("clear")

    def test_get_nontag_texts(self):
        logger.debug("test_get_nontag_texts")
        test_strings = [
            "タグが全く含まれていない文字列",
            "<h1>先頭と最後がタグである文字列</h1>",
            "<h1>上記に加えて</h1>その後も文字列があったりする",
            "<h1>さらに</h1><ul><li>リスト</li></ul>とか？"
        ]
        expected_strings = [
            [("タグが全く含まれていない文字列", (0, 15))],
            [("先頭と最後がタグである文字列", (4, 18))],
            [("上記に加えて", (4, 10)),
             ("その後も文字列があったりする", (15, 29))],
            [("さらに", (4, 7)),
             ("リスト", (20, 23)),
             ("とか？", (33, 36))]
        ]
        for (string, expected) in zip(test_strings, expected_strings):
            self.assertEqual(
                parser._get_nontag_texts(string),
                expected)
        logger.debug("clear")

    def test_separate_string_by_rules(self):
        logger.debug("test_separate_string_by_rules")
        # rules = [
        #     r"[ 　][ 　]+",
        #     re.compile("[1-9][0-9]*px")
        # ]
        rules = parser.SeparateRules().rules
        test_strings = [
            "こんな　　空白を含む 　文",
            "タイトル 60px こんな感じ　　とか",
            "ねこ 5px  これくらいで",
            "句点。　　文を。る　　必要がある。"
        ]
        expected_strings = [
            [("@0@A", "こんな", (4, 7)),
             ("@0@B", "　　", (7, 9)),
             ("@0@C", "空白を含む", (9, 14)),
             ("@0@D", " 　", (14, 16)),
             ("@0@E", "文", (16, 17))],
            [(None, "タイトル ", (0, 5)),
             (None, "60px", (5, 9)),
             (None, " こんな感じ", (9, 15)),
             (None, "　　", (15, 17)),
             (None, "とか", (17, 19))],
            [(None, "ねこ ", (0, 3)),
             (None, "5px", (3, 6)),
             (None, "  ", (6, 8)),
             (None, "これくらいで", (8, 14))],
            [(None, "句点。", (0, 3)),
             (None, "　　", (3, 5)),
             (None, "文を。", (5, 8)),
             (None, "る", (8, 9)),
             (None, "　　", (9, 11)),
             (None, "必要がある。", (11, 17))
             ],
        ]
        for (string, expected) in zip(test_strings, expected_strings):
            if string == "こんな　　空白を含む 　文":
                self.assertEqual(
                    parser.separate_string_by_rules(
                        string, rules, offset=4, prefix="@0"
                    ),
                    expected
                )
            else:
                self.assertEqual(
                    parser.separate_string_by_rules(
                        string, rules, offset=0
                    ),
                    expected
                )
        logger.debug("clear")

    def test_factor_out_and_exclude_by_rules(self):
        logger.debug("test_factor_out_and_exclude_by_rules")
        test_strings = [
            "彼は「僕は何もやっていない」とだけ語る。",
            "「呑みすぎたのは」に続き「あなたのせいよ」となる",
            "これは『見上げる「だけの」セカイ』の話。",
            "参考： http://sample.net/some/text 記事タイトル",
            "あ「ねこ http://sample.com」 from https://sample.com なり"
        ]
        expected_strings = [
            ("彼は「@0@0」とだけ語る。",
             [("@0@0", "僕は何もやっていない", (4, 14))],
             [],
             [(14, 6)]),
            ("「@0@0」に続き「@0@1」となる",
             [("@0@0", "呑みすぎたのは", (2, 9)),
              ("@0@1", "あなたのせいよ", (14, 21))],
             [],
             [(9, 3), (21, 6)]),
            ("これは『@0@0』の話。",
             [("@0@0", "見上げる「だけの」セカイ", (5, 17))],
             [],
             [(17, 8)]),
            ("参考： @0&0 記事タイトル",
             [],
             [("@0&0", "http://sample.net/some/text", (5, 32))],
             [(32, 23)]),
            ("あ「@0@0」 from @0&0 なり",
             [("@0@0", "ねこ http://sample.com", (3, 23))],
             [("@0&0", "https://sample.com", (30, 48))],
             [(23, 16), (48, 30)])
        ]
        rule = parser.ExcludeRules().rule
        for (string, expected) in zip(test_strings, expected_strings):
            self.assertEqual(
                parser.factor_out_and_exclude_by_rules(
                    string, string, rule, offset=1, prefix="@0"
                ),
                expected
            )
        logger.debug("clear")
