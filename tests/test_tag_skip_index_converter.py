from unittest import TestCase

from shinra_util import tag_skip_index_converter


class TestTagSkipIndexConverter(TestCase):
    def setUp(self):
        self.html = """<h1>Test</h1>doc
<ul>
<li>item1</li>
<li>
item2
</ul>
        """

    def test_get_tag_skip_index(self):
        converter = \
            tag_skip_index_converter.TagSkipIndexConverter(self.html)
        self.assertEqual(
            converter.get_tag_skip_index(4),
            0
        )
        self.assertEqual(
            converter.get_tag_skip_index(21),
            8
        )
        self.assertEqual(
            converter.get_tag_skip_index(43),
            17
        )

    def test_get_simple_index(self):
        converter = \
            tag_skip_index_converter.TagSkipIndexConverter(self.html)
        self.assertEqual(
            converter.get_simple_index(0),
            4
        )
        self.assertEqual(
            converter.get_simple_index(12),
            29
        )
        self.assertEqual(
            converter.get_simple_index(29),
            60
        )


class TestLineTagSkipIndexConverter(TestCase):
    def setUp(self):
        self.html = """<h1>Test</h1>doc
<ul>
<li>item<it>1</it></li>
<li>
item2
</ul>
        """

    def test_get_line_skip_tag_index(self):
        converter = \
            tag_skip_index_converter.LineTagSkipIndexConverter(self.html)
        self.assertEqual(
            converter.get_line_skip_tag_index(4),
            (0, 0)
        )
        self.assertEqual(
            converter.get_line_skip_tag_index(21),
            (1, 0)
        )
        self.assertEqual(
            converter.get_line_skip_tag_index(34),
            (2, 4)
        )

    def test_get_simple_index(self):
        converter = \
            tag_skip_index_converter.LineTagSkipIndexConverter(self.html)
        self.assertEqual(
            converter.get_simple_index((0, 0)),
            4
        )
        self.assertEqual(
            converter.get_simple_index((1, 0)),
            21
        )
        self.assertEqual(
            converter.get_simple_index((2, 4)),
            34
        )
